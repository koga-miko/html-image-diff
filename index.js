import { chromium } from "playwright";
import fs from "fs/promises";
import pngJs from "pngjs";
import pixelmatch from "pixelmatch";
import sharp from "sharp";
import path from "path";

const TEMP_PNG = "_temp.png";

function resize(width, height, srcFileName, dstFileName, isFromTop = true) {
  const position = isFromTop ? "top" : "bottom";
  return new Promise((resolve, reject) => {
    sharp(srcFileName)
      .resize(width, height, {
        fit: "contain",
        position: position,
      })
      .toFile(dstFileName, (err, info) => {
        if (err) {
          reject(new Error(`fail resize.${err}`));
          return;
        }
        resolve();
      });
  });
}

function diffPagesByPixelMatch(
  url1,
  url2,
  src1PngFileName,
  src2PngFileName,
  outFileName,
  isFromTop = true
) {
  return new Promise((resolve, reject) => {
    (async () => {
      let num = 0;
      try {
        // Setup
        const browser = await chromium.launch();
        const page1 = await browser.newPage();
        const page2 = await browser.newPage();
        // The actual interesting bit
        await page1.goto(url1);
        await page2.goto(url2);
        await page1.screenshot({
          path: src1PngFileName,
          fullPage: true,
        });
        await page2.screenshot({
          path: src2PngFileName,
          fullPage: true,
        });
        // Teardown
        await browser.close();
        // start matching
        const PNG = pngJs.PNG;
        let img1 = PNG.sync.read(await fs.readFile(src1PngFileName));
        let img2 = PNG.sync.read(await fs.readFile(src2PngFileName));
        const imgWidth = Math.max(img1.width, img2.width);
        const imgHeight = Math.max(img1.height, img2.height);
        if (img1.width !== imgWidth || img1.height !== imgHeight) {
          const tmpPngName = src1PngFileName + ".tmp.png";
          await fs.rename(src1PngFileName, tmpPngName);
          await resize(
            imgWidth,
            imgHeight,
            tmpPngName,
            src1PngFileName,
            isFromTop
          );
          img1 = PNG.sync.read(await fs.readFile(src1PngFileName));
          await fs.rm(tmpPngName);
        }
        if (img2.width !== imgWidth || img2.height !== imgHeight) {
          const tmpPngName = src2PngFileName + ".tmp.png";
          await fs.rename(src2PngFileName, tmpPngName);
          await resize(
            imgWidth,
            imgHeight,
            tmpPngName,
            src2PngFileName,
            isFromTop
          );
          img2 = PNG.sync.read(await fs.readFile(src2PngFileName));
          await fs.rm(tmpPngName);
        }
        const diff = new PNG({ width: imgWidth, height: imgHeight });
        num = pixelmatch(img1.data, img2.data, diff.data, imgWidth, imgHeight, {
          threshold: 0.2,
          alpha: 0.3,
          diffColorAlt: [0, 0, 255],
        });
        await fs.writeFile(outFileName, PNG.sync.write(diff));
      } catch (err) {
        reject(new Error(`Fail: ${err}`));
        return;
      }
      resolve(num);
    })();
  });
}

// File names for main fucntion.
const TOP_DIR = "./out/top";
const BTM_DIR = "./out/btm";
const SRC1_PNG = "src1.png";
const SRC2_PNG = "src2.png";
const DIFF_PNG = "diff.png";

function main() {
  if (process.argv.length < 4) {
    console.log("[contents-diff-trial] node.js index.js src1Url src2Url");
    return;
  }
  console.log("[contents-diff-trial] src1Url = " + process.argv[2]);
  console.log("[contents-diff-trial] src2Url = " + process.argv[3]);

  [
    { dirname: TOP_DIR, isFromTop: true },
    //    { dirname: BTM_DIR, isFromTop: false },
  ].forEach(async (info) => {
    try {
      await fs.mkdir(info.dirname, { recursive: true });
      console.log(
        "[contents-diff-trial] created the directory: " + info.dirname
      );
    } catch (e) {
      console.log(
        "[contents-diff-trial] maybe already exist the directory: " +
          info.dirname
      );
    }
    try {
      const num = await diffPagesByPixelMatch(
        process.argv[2],
        process.argv[3],
        path.join(info.dirname, SRC1_PNG),
        path.join(info.dirname, SRC2_PNG),
        path.join(info.dirname, DIFF_PNG),
        info.isFromTop
      );
      console.log(
        `[contents-diff-trial] number of mismatched pixels: ${num} ... success!!!`
      );
    } catch (e) {
      console.log(`[contents-diff-trial] error!!! ${e}`);
    }
  });
}

main();
